package util;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



import model.Animal;
import model.Istoric;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setup() {
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("Tema4");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void beginTransaction() {
		entityManager.getTransaction().begin();
	}

	public void saveAnimal(Animal a) {
		entityManager.persist(a);
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void stop() {
		entityManager.close();
	}
	/**
	 * get all animal list from datebase
	 * @return
	 */
	public List<Animal> animalList() {
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
	/**
	 * get one animal that have name equal with parameter name
	 * @param name
	 * @return
	 */
	public Animal getAnimal(String name) {
		Animal animal = (Animal)entityManager.createQuery("SELECT a FROM Animal a WHERE a.name="+"\""+name+"\"",Animal.class).getSingleResult();
		return animal;
	}
	/**
	 * get one Personalmedical that have name equal with parameter name
	 * @param name
	 * @return
	 */
	public Personalmedical getMedic(String name) {
		Personalmedical med = (Personalmedical)entityManager.createQuery("SELECT a FROM Personalmedical a WHERE a.nume="+"\""+name+"\"", Personalmedical.class).getSingleResult();
		return med;
	}
	/**
	 * we get list of all historic
	 * @return
	 */
	public List<Istoric> istoricList() {
		List<Istoric> animalList = (List<Istoric>)entityManager.createQuery("SELECT a FROM Istoric a",Istoric.class).getResultList();
		return animalList;
	}
	/**
	 * we get historic for one animal with id given in parameter
	 * @param id
	 * @return
	 */
	public List<Istoric> istoric(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(id);
		String strI = sb.toString();
		List<Istoric> animalList = (List<Istoric>)entityManager.createQuery("SELECT a FROM Istoric a WHERE a.idAnimal="+strI,Istoric.class).getResultList();
		return animalList;
	}
	public List<Animal> animal(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(id);
		String strI = sb.toString();
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a WHERE a.idAnimal="+strI,Animal.class).getResultList();
		return animalList;
	}
	/**
	 * get one animal that have id equal with our parameterr id
	 * @param id
	 * @return
	 */
	public Animal animalById(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(id);
		String strI = sb.toString();
		Animal animal = (Animal)entityManager.createQuery("SELECT a FROM Animal a WHERE a.idAnimal="+strI,Animal.class).getSingleResult();
		return animal;
	}
	/**
	 * we get the map of animals
	 * @return
	 */
	public Map<Integer,Animal> animalMap() {
		
		Map<Integer,Animal> anim=new HashMap<Integer, Animal>();
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a ",Animal.class).getResultList();
		for(Animal a:animalList)
			anim.put(a.getIdAnimal(),a);
		return anim;
	}
	/**
	 * we get list of Personalmedical
	 * @return
	 */
	public List<Personalmedical> persMedList() {
		List<Personalmedical> animalList = (List<Personalmedical>)entityManager.createQuery("SELECT a FROM Personalmedical a", Personalmedical.class).getResultList();
		return animalList;
	}
	public List<Personalmedical> persMedList(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(id);
		String strI = sb.toString();
		List<Personalmedical> animalList = (List<Personalmedical>)entityManager.createQuery("SELECT a FROM Personalmedical a WHERE a.idPersonalMedical="+strI, Personalmedical.class).getResultList();
		return animalList;
	}
	/**
	 * we get one Personalmedical that have id equal with parameter
	 * @param id
	 * @return
	 */
	public Personalmedical persMedById(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(id);
		String strI = sb.toString();
		Personalmedical persMed = (Personalmedical)entityManager.createQuery("SELECT a FROM Personalmedical a WHERE a.idPersonalMedical="+strI, Personalmedical.class).getSingleResult();
		return persMed;
	}
	/**
	 * we get list of Programare
	 * @return
	 */
	public List<Programare> programariList() {
		List<Programare> animalList = (List<Programare>)entityManager.createQuery("Select a from Programare a",Programare.class).getResultList();
		return animalList;
	}
	/**
	 * we get list of Programare where id of medic is equal with our parameter
	 * @return
	 */
	public List<Programare> programariList(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(id);
		String strI = sb.toString();
		List<Programare> animalList = (List<Programare>)entityManager.createQuery("Select a from Programare a WHERE a.idPersonalMedical="+strI,Programare.class).getResultList();
		return animalList;
	}
	/*public void setUp() throws Exception {
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("Tema4");
		entityManager = entityManagerFactory.createEntityManager();
		entityManagerFactory=Persistence.createEntityManagerFactory("Tema4");
		entityManager=entityManagerFactory.createEntityManager();
	}*/
	/**
	 * The saves function of animals
	 * @param animal
	 */
	/*public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}*/
	/**
	 * The save function of medical staff
	 * @param persMed
	 */
	public void savePersMed(Personalmedical persMed) {
		entityManager.persist(persMed);
	}
	/**
	 *  The save function of appointment
	 * @param prog
	 */
	public void saveProg(Programare prog) {
		entityManager.persist(prog);
	}
	/**
	 * Hear we delete an animal by a parameter which is type animal
	 * @param animal
	 */
	public void deleteAnimal(Animal animal) {
		entityManager.remove(animal);
	}
	/**
	 * Hear we delete a medical personnel by a parameter which is type Personalmedical
	 * @param persMed
	 */
	public void deletePersMed(Personalmedical persMed) {
		entityManager.remove(persMed);
	}
	/**
	 * Hear we delete an appointment by a parameter which is type Programare
	 * @param prog
	 */
	public void deleteProg(Programare prog) {
		entityManager.remove(prog);
	}
	/**
	 *  Hear we delete an animal by a parameter which is id of an animal
	 * @param animal
	 */
	public void deleteAnimalById(int animal) {
		Animal anim=entityManager.find(Animal.class, animal);
		entityManager.remove(anim);
	}
	/**
	 * Hear we delete a medical personnel by a parameter which is id of a medical personnel
	 * @param persMed
	 */
	public void deletePersMedById(int persMed) {
		Personalmedical anim=entityManager.find(Personalmedical.class, persMed);
		entityManager.remove(anim);
	}
	/**
	 * Hear we delete an appointment by a parameter which is id of an appointment
	 * @param prog
	 */
	public void deleteProgById(int prog) {
		Programare anim=entityManager.find(Programare.class, prog);
		entityManager.remove(anim);
	}
	/**
	 * Update an animal with a new name and type and we give the id of animal that we want to modify
	 * @param animal
	 * @param newName
	 * @param newTipAnimal
	 */
	public void updateAnimalById(int animal,String newName,String newTipAnimal) {
		Animal anim=entityManager.find(Animal.class, animal);
		//anim.setIdAnimal(newId);
		anim.setName(newName);
		anim.setTipAnimal(newTipAnimal);
		//entityManager.merge(anim);
	}
	/**
	 * Update a medical personnel with a new name and type and we give the id of medical personnel that we want to modify
	 * @param persMed
	 * @param newName
	 * @param newTip
	 */
	public void updatePersMedById(int persMed,String newName,String newTip) {
		Personalmedical anim=entityManager.find(Personalmedical.class, persMed);
		anim.setNume(newName);
		anim.setTipPersonal(newTip);
	}
	/**
	 * Update an appointment with a new id for animal, a new id for medical personnel and a new data and we give the id of appointment that we want to modify
	 * @param prog
	 * @param newIdAnimal
	 * @param newIdPErsMedical
	 * @param newData
	 */
	public void updateProgById(int prog,int newIdAnimal,int newIdPErsMedical,Date newData) {
		Programare anim=entityManager.find(Programare.class, prog);
		anim.setData(newData);
		anim.setIdAnimal(newIdAnimal);
		anim.setIdPersonalMedical(newIdPErsMedical);
	}
	public void startTransaction() {
		entityManager.getTransaction().begin();		
	}
	/*public void commitTransaction() {
		entityManager.getTransaction().commit();
	}/*
	public void closeEntityManager() {
		entityManager.close();
	}
	/**
	 * Print all animals form database
	 */
	public void printAllAnimalsFormBD() {
		List<Animal> results=(List<Animal>)entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class).getResultList();
		for (Animal animal:results) {
			System.out.println("Animal : "+animal.getName() + " has ID: "+animal.getIdAnimal());
			
		}
	}
	/**
	 * Print all medical personnel from database
	 */
	public void printAllPersMedicalFormBD() {
		List<Personalmedical> results=entityManager.createNativeQuery("Select * from PetShop.Personalmedical", Personalmedical.class).getResultList();
		for (Personalmedical persMed:results) {
			System.out.println("Personal medical nume : "+persMed.getNume() + " has ID: "+persMed.getIdPersonalMedical() + " tipul: "+ persMed.getTipPersonal());
			
		}
	}
	/*
	 * Print all appointments from database
	 */
	/*public void printAllProgramariFormBD() {
		List<Programare> results=entityManager.createNativeQuery("Select * from PetShop.Programare",Programare.class).getResultList();
		for (Programare prog:results) {
			System.out.println("ID Programare : "+prog.getIdprogramare() + " ID animal: "+prog.getIdAnimal() + " ID Personal Medical " + prog.getIdPersonalMedical()+ " Data" + prog.getData());
			
		}
	}
	/**
	 * Sort all appointments from database by data and print the id of appointment, the name of the animal and the name of the medical staff
	 */
	/*public void sortAllProgramariFormBD() {
		List<Programare> results=entityManager.createNativeQuery("Select * from PetShop.Programare",Programare.class).getResultList();
		results.sort(Comparator.comparing(Programare::getData));
		for (Programare prog:results) {
			System.out.println("ID Programare : "+prog.getIdprogramare() + " nume animal: "+entityManager.find(Animal.class,prog.getIdAnimal()).getName() + " nume Personal Medical " + entityManager.find(Personalmedical.class,prog.getIdPersonalMedical()).getNume()+ " Data" + prog.getData());
			
		}
	}*/
	/*public List<Animal> animalList() {
		List<Animal> results=entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class).getResultList();
		return results;
	}*/
}
