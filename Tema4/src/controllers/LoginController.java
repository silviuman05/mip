package controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class LoginController implements Initializable {
	@FXML PasswordField pass;
	@FXML TextField user;
	@FXML Label pas;
	@FXML Label us;
	@FXML Button log;
	@FXML public void login(ActionEvent event) {
		try {
			Socket socket=new Socket("localhost", 5000);
			BufferedReader echoes=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringEcho=new PrintWriter(socket.getOutputStream(),true);
			//Scanner scanner=new Scanner(System.in);
			String echoString;
			String response;
			//do {
			//	System.out.println("Enter string to be echo");
				echoString=user.getText();
				stringEcho.println(echoString);
				echoString=pass.getText();
				 //us.setText( user.getText());
				//pas.setText( pass.getText());
				stringEcho.println(echoString);
				//if(!echoString.equals("exit")) {
				//	response=echoes.readLine();
				//}
		//	} while (!echoString.equals("exit"));
				stringEcho.flush();
				response=echoes.readLine();
				if(response.equals("Login Failed"))
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("ALERT");
						alert.setHeaderText("Invlide user or password");
						alert.showAndWait().ifPresent(rs -> {
						    if (rs == ButtonType.OK) {
							        System.out.println("Pressed OK.");
							    }
							});
					}
				else {
					Parent root=FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
					Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
					Scene logScene=new Scene(root,700,700);
					
					Stage window =(Stage)((Node)event.getSource()).getScene().getWindow();
					window.setX(primaryScreenBounds.getMinX()+200  );
					window.setY(primaryScreenBounds.getMinY() );
					window.setScene(logScene);
					window.show();
				}
				 System.out.println("This is the response: " + response);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
}
