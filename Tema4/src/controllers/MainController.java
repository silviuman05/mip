package controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.util.Collections;
import java.awt.Event;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.fabric.xmlrpc.base.Data;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.*;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import model.Animal;
import main.Main;
import model.Istoric;
import model.Personalmedical;
import model.Programare;
import singleton.Singleton;
import util.DatabaseUtil;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Label;
import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;

public class MainController implements Initializable {
	@FXML
	 private ListView<String> mainListView;
	@FXML
	 private ListView<String> listView;
	@FXML
	 private Button addAppointment;
	@FXML ImageView img;
	public int idMed=0;
	public int idAnimal=0;
	@FXML Label nameLabel;
	@FXML Label ageLabel;
	@FXML Label greutateLabel;
	@FXML Label stapanLabel;
	@FXML Label tipLabel;

	@FXML ListView<String> listIstoric;
	/**
	 * deschiderea ferestrei pentru adaugarea unei noi programari
	 * @param e
	 * @throws IOException
	 */
	@FXML
	void addAppoinmentClicked(ActionEvent e) throws IOException {
		 Label secondLabel = new Label("Hello");
		// FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/controllers/MainView2.fxml"));
		// Parent root = fxmlLoader.load();
		 BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView2.fxml"));
         StackPane secondaryLayout = new StackPane();
         secondaryLayout.getChildren().add(secondLabel);   
         Scene secondScene = new Scene(root, 500, 500);
         Stage secondStage = new Stage();
         secondStage.setTitle("Second Stage");
         secondStage.setScene(secondScene);
         secondStage.show();
	}
	
/**
 * populate listView with doctors
 * @throws IOException
 */
	public void populateListView() throws IOException {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Personalmedical> persMedDBList = (List<Personalmedical>)db.persMedList();
		ObservableList<Personalmedical> animalList = FXCollections.observableArrayList(persMedDBList);
		ObservableList<String> copy =  FXCollections.observableArrayList();
		 listView.setItems(getPersMedNameObservableList(animalList));
		
        listView.refresh();
        
		
		db.stop();
	}
	/**
	 * populate mainListView with all appointments
	 * @throws IOException
	 */
	public void populateMainListViewWithData() throws IOException {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Programare> programDB = (List<Programare>)db.programariList(idMed);
		Collections.sort(programDB,(p1,p2)->p1.getData().compareTo(p2.getData()));
		ObservableList<Programare> programare = FXCollections.observableArrayList(programDB);
		ObservableList<String> copy =  FXCollections.observableArrayList();
		for(int i =0 ; i < getProgramariDataObservableList(programare).size(); i++)
			copy.add(getProgramariDataObservableList(programare).get(i).toString());
		mainListView.setItems( copy );
		mainListView.refresh();
        
		
		db.stop();
	}
	/**
	 * show one animal one scene after was selected from mainview
	 * @throws IOException
	 */
	public void AnimalShow() throws IOException {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		Map<Integer,Animal> anim=db.animalMap();
		Animal a=anim.get(idAnimal);
		nameLabel.setText(a.getName());
		int age=a.getAge();
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(age);
		String strI = sb.toString();
		ageLabel.setText(strI);
		int greutate=a.getGreutate();
		StringBuilder sb2 = new StringBuilder();
		sb2.append("");
		sb2.append(greutate);
		String strI2 = sb2.toString();
		greutateLabel.setText(strI2);
		stapanLabel.setText(a.getNumeStapan());
		tipLabel.setText(a.getTipAnimal());
		
		InputStream is=new ByteArrayInputStream(a.getPozaAn());
        OutputStream os=new FileOutputStream(new File("img.jpg"));
        byte [] content= new byte[1024];
        int size=0;
            while ((size=is.read(content))!=-1){
                os.write(content, 0, size);
            }
        os.close();
        is.close();
        javafx.scene.image.Image image1=new Image("file:img.jpg", img.getFitWidth(), img.getFitHeight(), true, true);
        img.setImage(image1);
        img.setPreserveRatio(true);
        List<Istoric> istoric=( List<Istoric>)db.istoric(idAnimal);
        ObservableList<Istoric> animalList = FXCollections.observableArrayList(istoric);
        listIstoric.setItems(getIstoricObservableList(animalList));
		
	}
	public ObservableList<String> getAnimalNameObservableList(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a: animals) {
			names.add(a.getName());
		}
		return names;
	}
	public ObservableList<String> getPersMedNameObservableList(List<Personalmedical> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Personalmedical a: animals) {
			names.add(a.getNume());
		}
		return names;
	}
	public ObservableList<String> getIstoricObservableList(List<Istoric> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Istoric a: animals) {
			names.add(a.getDiagnostic());
		}
		return names;
	}
	
	public ObservableList<Date> getProgramariDataObservableList(List<Programare> animals) {
		ObservableList<Date> names = FXCollections.observableArrayList();
		for (Programare a: animals) {
			names.add(a.getData(idMed));
		}
		return names;
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateListView();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
			/**
			 * aflare id animal cand se face click pe un elment din lista
			 * @param event
			 * @throws IOException
			 */
			@FXML public void handle(MouseEvent event) throws IOException 
			{
				DatabaseUtil db = new DatabaseUtil();
				db.setup();
				db.beginTransaction();
				List<Programare> programareListBD = (List<Programare>)db.programariList(idMed);
				System.out.println("Selected index: "+mainListView.getSelectionModel().getSelectedIndex());	
				idAnimal=programareListBD.get(mainListView.getSelectionModel().getSelectedIndex()).getIdAnimal();
				AnimalShow();				 
			}
			/**
			 * aflare id medic cand se face click pe un elment din lista
			 * @param event
			 * @throws IOException
			 */
			@FXML public void first(MouseEvent event) throws IOException 
			{
				Singleton pers=Singleton.getInstance();
				idMed=pers.getMedId(listView.getSelectionModel().getSelectedIndex());
				//idMed=persMedDBList.get(listView.getSelectionModel().getSelectedIndex()).getIdPersonalMedical();
				System.out.println("Selected index: "+listView.getSelectionModel().getSelectedIndex());	
				populateMainListViewWithData() ;
				//listView.setVisible(false);
				
			}

			/**
			 * evenimentul butonului pentru stergerea unei programari
			 * @param event
			 * @throws IOException
			 */
			@FXML public void deleteProgramare(ActionEvent event) throws IOException {
				DatabaseUtil db = new DatabaseUtil();
				db.setup();
				db.beginTransaction();
				List<Programare> programare = (List<Programare>)db.programariList(idMed);
				System.out.println("Selected index: 0");	
				Programare pr=programare.get(mainListView.getSelectionModel().getSelectedIndex());
				db.deleteProg(pr);
				db.commitTransaction();
		 		db.stop();
		 		populateMainListViewWithData() ;
			}

			/**
			 * deschiderea unei noi ferestre pentru a aduga un nou animal
			 * @param event
			 * @throws IOException
			 */
			@FXML public void addAnimal(ActionEvent event) throws IOException {
				Label secondLabel = new Label("Hello");
				Parent root =FXMLLoader.load(getClass().getResource("/controllers/AddAnimal.fxml"));
		         StackPane secondaryLayout = new StackPane();
		         secondaryLayout.getChildren().add(secondLabel);   
		         Scene secondScene = new Scene(root, 500, 500);
		         Stage secondStage = new Stage();
		         secondStage.setTitle("Second Stage");
		         secondStage.setScene(secondScene);
		         secondStage.show();
				
			}


			@FXML public void closeWindow(ActionEvent event) {
				
				 Stage stage = (Stage) mainListView.getScene().getWindow();
				    stage.close();
			}
}
	

