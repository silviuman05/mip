package controllers;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;

import model.Animal;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddController  implements Initializable{
	@FXML ComboBox<String> comboBoxMed;
	@FXML ComboBox<String> comboBoxAnimal;
	@FXML DatePicker data;
	@FXML TextField time;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		 DatabaseUtil db = new DatabaseUtil();
	 		db.setup();
	 		db.beginTransaction();
	 		List<Personalmedical> persMedDBList = (List<Personalmedical>)db.persMedList();
	 	
	 		ObservableList<Personalmedical> medList = FXCollections.observableArrayList(persMedDBList);
	 		 comboBoxMed.getItems().addAll(getPersMedNameObservableList(medList));
	 		List<Animal> animalDBList = (List<Animal>)db.animalList();
	 		ObservableList<Animal> animalList = FXCollections.observableArrayList(animalDBList);
	 		 comboBoxAnimal.getItems().addAll(getAnimalNameObservableList(animalList));
		
	}
	public ObservableList<String> getPersMedNameObservableList(List<Personalmedical> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Personalmedical a: animals) {
			names.add(a.getNume());
		}
		return names;
	}
	public ObservableList<String> getAnimalNameObservableList(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a: animals) {
			names.add(a.getName());
		}
		return names;
	}
	/**
	 * function that save in database informations when save button is press
	 * @param event
	 */
	@FXML public void save(ActionEvent event) throws IOException {
		System.out.println(data.getValue());
		 LocalDate datePart = data.getValue();
		  LocalTime timePart = LocalTime.parse(time.getText());
		   LocalDateTime dt = LocalDateTime.of(datePart, timePart);
		   Instant instant = dt.atZone(ZoneId.systemDefault()).toInstant();
		   Programare prog=new Programare();
		   DatabaseUtil db = new DatabaseUtil();
	 		db.setup();
	 		db.beginTransaction();
	 		List<Programare> progDBList = (List<Programare>)db.programariList();
	 		System.out.println(instant.toString());
	 		prog.setIdprogramare(progDBList.size()+1);
	 		prog.setData(Date.from(instant));
	 		prog.setIdAnimal(db.getAnimal(comboBoxAnimal.getValue()).getIdAnimal());
	 		prog.setIdPersonalMedical(db.getMedic(comboBoxMed.getValue()).getIdPersonalMedical()); 
	 		db.saveProg(prog);
	 		db.commitTransaction();
	 		db.stop();

	}
	/**
	 * function that close add window whene exit button is press
	 * @param event
	 */
	@FXML public void exit(ActionEvent event) {
		 Stage stage = (Stage) comboBoxMed.getScene().getWindow();
		    // do what you have to do
		    stage.close();
	}
}
