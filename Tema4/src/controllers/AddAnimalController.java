package controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;


public class AddAnimalController implements Initializable {
	@FXML TextField greutateT;
	@FXML TextField stapanT;
	@FXML TextField varstaT;
	@FXML TextField TipT;
	@FXML TextField numeT;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@FXML public void exitAnimalWindow(ActionEvent event) {
		 Stage stage = (Stage) greutateT.getScene().getWindow();
		    stage.close();
	}

	@FXML public void saveAnimal(ActionEvent event) {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Animal> animal = (List<Animal>)db.animalList();
		Animal an=new Animal();
		an.setIdAnimal(animal.size()+1);
		an.setName(numeT.getText());
		an.setNumeStapan(stapanT.getText());
		an.setTipAnimal(TipT.getText());
		an.setAge(Integer.parseInt(varstaT.getText()));
		an.setGreutate(Integer.parseInt(greutateT.getText()));
		db.saveAnimal(an);
		db.commitTransaction();
 		db.stop();
	}

}
