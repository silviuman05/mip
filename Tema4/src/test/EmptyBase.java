package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.sun.net.httpserver.Authenticator.Success;

import model.Animal;
import model.Istoric;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;

class EmptyBase {

	@Test
	void medListTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Personalmedical> persMedDBList = (List<Personalmedical>)db.persMedList();
		if(persMedDBList.isEmpty())
			assertNull(persMedDBList);
		
	}
	@Test
	void medByIdTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		Personalmedical persMedDB = (Personalmedical)db.persMedById(12);
		assertFalse(persMedDB.getIdPersonalMedical()!=12);
		
	}
	@Test
	void animalListTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Animal>animalDBList = (List<Animal>)db.animalList();
		if(animalDBList.isEmpty())
			assertNull(animalDBList);
		
	}
	@Test
	void animalByNameTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		Animal animalDB = (Animal)db.getAnimal("Tom");
		assertFalse(animalDB.getIdAnimal()!=1);
	}
	@Test
	void animalByIdTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		Animal animalDBList = (Animal)db.animalById(2);
		assertFalse(animalDBList.getIdAnimal()!=2);
	}
	@Test
	void progListTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Programare> progDBList = (List<Programare>)db.programariList();
		if(progDBList.isEmpty())
			assertNull(progDBList);
		
	}
	@Test
	void progListByIdMedTest() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Programare> progDBList = (List<Programare>)db.programariList(12);
		for(Programare x:progDBList)
			assertFalse(x.getIdPersonalMedical()!=12);
		
	}
	@Test
	void  istoricListTest()
	{
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Istoric> istDBList = (List<Istoric>)db.istoricList();
		if(istDBList.isEmpty())
			assertNull(istDBList);
	}
	void  istoricListByIdAimalTest()
	{
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Istoric> istDBList = (List<Istoric>)db.istoric(1);
		for(Istoric x:istDBList)
			assertFalse(x.getIdAnimal()!=1);
	}
	@Test
	void  animalMapTest()
	{
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		Map<Integer,Animal> anim=db.animalMap();
		if(anim.isEmpty())
			assertNull(anim);
	}
}
