package singleton;

import java.util.ArrayList;
import java.util.List;

import model.Personalmedical;
import util.DatabaseUtil;

public class Singleton {
	private static Singleton instance = new Singleton();
	private List<Personalmedical> persMedBD=new ArrayList<Personalmedical>();;
	private Singleton() 
    { 
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		persMedBD = (List<Personalmedical>)db.persMedList();
    } 
	public int  getMedId(int id)
	{
		return persMedBD.get(id).getIdPersonalMedical();
	}
	public static Singleton getInstance() 
	    { 
	        return instance; 
	    } 

}
