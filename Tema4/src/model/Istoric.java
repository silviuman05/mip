package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the istoric database table.
 * 
 */
@Entity
@NamedQuery(name="Istoric.findAll", query="SELECT i FROM Istoric i")
public class Istoric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idIstoric;

	private String diagnostic;

	private int idAnimal;

	private int idPersonalMedi;

	public Istoric() {
	}

	public int getIdIstoric() {
		return this.idIstoric;
	}

	public void setIdIstoric(int idIstoric) {
		this.idIstoric = idIstoric;
	}

	public String getDiagnostic() {
		return this.diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getIdPersonalMedi() {
		return this.idPersonalMedi;
	}

	public void setIdPersonalMedi(int idPersonalMedi) {
		this.idPersonalMedi = idPersonalMedi;
	}

}