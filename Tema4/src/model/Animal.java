package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	private int age;

	private int greutate;

	private String name;

	private String numeStapan;

	@Lob
	private byte[] pozaAn;

	private String tipAnimal;

	//bi-directional many-to-one association to Programare
	//@OneToMany(mappedBy="animal")
	//private List<Programare> programares;

	//bi-directional many-to-one association to Istoric
	//@OneToMany(mappedBy="animal")
	//private List<Istoric> istorics;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getGreutate() {
		return this.greutate;
	}

	public void setGreutate(int greutate) {
		this.greutate = greutate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumeStapan() {
		return this.numeStapan;
	}

	public void setNumeStapan(String numeStapan) {
		this.numeStapan = numeStapan;
	}

	public byte[] getPozaAn() {
		return this.pozaAn;
	}

	public void setPozaAn(byte[] pozaAn) {
		this.pozaAn = pozaAn;
	}

	public String getTipAnimal() {
		return this.tipAnimal;
	}

	public void setTipAnimal(String tipAnimal) {
		this.tipAnimal = tipAnimal;
	}

	/*public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

	public List<Istoric> getIstorics() {
		return this.istorics;
	}

	public void setIstorics(List<Istoric> istorics) {
		this.istorics = istorics;
	}

	public Istoric addIstoric(Istoric istoric) {
		getIstorics().add(istoric);
		istoric.setAnimal(this);

		return istoric;
	}

	public Istoric removeIstoric(Istoric istoric) {
		getIstorics().remove(istoric);
		istoric.setAnimal(null);

		return istoric;
	}*/

}