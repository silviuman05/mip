package model;

import java.io.Serializable;
import javax.persistence.*;

import org.eclipse.persistence.internal.sessions.DirectCollectionChangeRecord.NULL;

import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramare;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	private int idAnimal;

	private int idPersonalMedical;

	public Programare() {
	}

	public int getIdprogramare() {
		return this.idprogramare;
	}

	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}
	public Date getData() {
		
		return data;
	}
	public Date getData(int id) {
		Date d =new Date();
		if(this.idPersonalMedical==id)
			{
				d=this.data;
			}
		return d;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

}